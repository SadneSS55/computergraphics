import javax.swing.*;
import java.awt.*;

public class Fractal extends JFrame {
    private final int width = 1280;
    private final int height = 1024;

    public Fractal() {
        this.setTitle("Pythagoras tree");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(width, height);
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.black);
        draw((Graphics2D) g, width / 2, height, -90, 20, ((double) (width + height) / 2) / 3);
    }

    public void draw(Graphics2D g, int a, int b, double angle, int depth, double size) {
        if (depth > 0) {
            int x2 = a + (int) (Math.cos(Math.toRadians(angle)) * size);
            int y2 = b + (int) (Math.sin(Math.toRadians(angle)) * size);
            g.drawLine(a, b, x2, y2);
            draw(g, x2, y2, angle + 90, depth - 1, size * 0.67);
            draw(g, x2, y2, angle - 90, depth - 1, size * 0.67);
        }
    }

    public static void main(String[] args) {
        Fractal fractal = new Fractal();
        fractal.setVisible(true);
        fractal.setBackground(Color.pink);
    }

}
